﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestDataGenerator.Data;

namespace TestDataGenerator.BL.Test
{
    public class RepositiryMock:IRepository
    {
        public void Init()
        {
            
        }

        public string GetRandomName()
        {
            return "Иван";
        }

        public string GetRandomSurname()
        {
            return "Иванов";
        }

        public string GetRandomPatronymic()
        {
            return "Иванович";
        }

        public string GetRandomUniqLogin()
        {
            return "ivan";
        }

        public string GetRandomEmailDomain()
        {
            return "test.ru";
        }
    }
}
