﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net.Http;
using Newtonsoft.Json;
using WeatherApp.Models;

namespace WeatherApp
{
    class WeatherAppAPI
    {
        public string API_KEY;

        public WeatherAppAPI(string apiKey)
        {
            API_KEY = apiKey;
        }

        public async void loadWeather(string cityID, string unitsFormat, Action<Weather> callback)
        {

            string url = "http://api.openweathermap.org/data/2.5/weather?appid=" + API_KEY + "&id=" + cityID + "&units=" + unitsFormat;

            using (HttpClient client = new HttpClient())
            using (HttpResponseMessage response = await client.GetAsync(url))
            using (HttpContent content = response.Content)
            {
                string result = await content.ReadAsStringAsync();
                Weather data = JsonConvert.DeserializeObject<Weather>(result);

                callback(data);
            }
        }

        public List<City> loadCities()
        {
            string url = "Resources/city.list.json";

            using (StreamReader r = new StreamReader(url))
            {
                string json = r.ReadToEnd();
                return JsonConvert.DeserializeObject<List<City>>(json);
            }
        }
    }
}
