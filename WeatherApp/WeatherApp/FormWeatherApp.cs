﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WeatherApp
{
    public partial class FormWeatherApp : Form
    {
        private WeatherAppAPI api;
        private Thread LoadingThread;

        public FormWeatherApp()
        {
            LoadingThread = new Thread(new ThreadStart(startForm));
            LoadingThread.Start();

            InitializeComponent();

            api = new WeatherAppAPI("dbf070d35a050b78592a22f5554b2317");

            regionSelect.DataSource = api.loadCities();
            regionSelect.DisplayMember = "name";
            regionSelect.ValueMember = "id";

            requestWeather();
        }

        public void requestWeather()
        {
            string region = regionSelect.SelectedValue.ToString();
            string units = getCheckedUnitsControl().Tag.ToString();

            api.loadWeather(region, units, showWeather);
        }

        public void showWeather(dynamic data)
        {
            string city = regionSelect.Text.ToString();
            string format = getCheckedUnitsControl().Text.ToString();

            resultBox.Text = String.Format("Today in {0} {1}{2}", city, data.main.temp, format);
        }

        public RadioButton getCheckedUnitsControl()
        {
            return unitsGroup.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked);
        }

        private void regionSelect_SelectionChangeCommitted(object sender, EventArgs e)
        {
            requestWeather();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            requestWeather();
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            requestWeather();
        }

        private void FormWeatherApp_Load(object sender, EventArgs e)
        {
            LoadingThread.Abort();
            this.Activate();
        }

        public void startForm()
        {
            Application.Run(new FormSplashScreen());
        }
    }
}
