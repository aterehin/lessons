﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherApp.Models
{
    public class Weather
    {
        public Main main{ get; set; }
    }

    public class Main
    {
        public float temp{ get; set; }
        public int humidity{ get; set; }
    }

}
